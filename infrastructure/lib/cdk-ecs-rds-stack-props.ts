import {StackProps} from "@aws-cdk/core";

export interface CdkEcsRdsStackProps extends StackProps {
    bastionKeyName: string;
    publicSubnet: {
        publicSubnetId: string;
        publicRouteTableId: string;
        publicSubnetAz: string;
    };
    vpcTargetTag: {
        'Name': string;
    };
    // TODO: Parameterize this ip address: from http://checkip.amazonaws.com/
    restrictedIp?: string;
}
