import * as cdk from '@aws-cdk/core';
import {PostgresqlRds} from "./postgresql-rds/postgesql-rds";
import {CdkEcsRdsStackProps} from "./cdk-ecs-rds-stack-props";

export class InfrastructureStack extends cdk.Stack {
    constructor(scope: cdk.Construct, id: string, props: CdkEcsRdsStackProps) {
        super(scope, id, props);
        new PostgresqlRds(this, props);
    }

}
