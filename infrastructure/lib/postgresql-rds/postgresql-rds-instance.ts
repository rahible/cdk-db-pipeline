import {
    Credentials,
    DatabaseInstance,
    DatabaseInstanceEngine,
    DatabaseInstanceProps,
    PostgresEngineVersion
} from "@aws-cdk/aws-rds";
import {CfnOutput, Stack} from "@aws-cdk/core";
import {CdkEcsRdsStackProps} from "../cdk-ecs-rds-stack-props";
import {ISecret} from "@aws-cdk/aws-secretsmanager";
import {
    Connections,
    InstanceClass,
    InstanceSize,
    InstanceType,
    Port,
    SecurityGroup,
    SubnetType,
    Vpc
} from "@aws-cdk/aws-ec2";

export class PostgresqlRdsInstance {
    constructor(private scope: Stack,
                private props: CdkEcsRdsStackProps,
                private databaseCredentialsSecret: ISecret,
                private securityGroups: SecurityGroup[]) {
    }

    create(): DatabaseInstance {
        // find vpc we want to put the RDS in
        const vpc = Vpc.fromLookup(this.scope, 'existingVpcRds', {
            isDefault: false,
            tags: this.props.vpcTargetTag
        });

        // Code Build Security Group -
        const codeBuildSecurityGroup = new SecurityGroup(this.scope, `${this.props.tags?.environment}CodeBuildSg`, {
            vpc,
            allowAllOutbound: true,
            description: 'Security Group for CodeBuild Container,RDS security group must set to ingress source to CodeBuildSecurityGroup',
            securityGroupName: `${this.props.tags?.environment}-codebuild-sg`
        });
        this.securityGroups.push(codeBuildSecurityGroup);

        // database security group
        const databaseSecurityGroup = new SecurityGroup(this.scope, `${this.props.tags?.environment}DatabaseSg`, {
            vpc,
            allowAllOutbound: true,
            description: 'security group postgresql',
            securityGroupName: `${this.props.tags?.environment}-database-sg`
        });

        // allow traffic from the bastion security group
        databaseSecurityGroup.connections.allowFrom(
            new Connections({
                securityGroups: this.securityGroups
            }),
            Port.tcp(5432),
            'Allow traffic to PostgreSQL default port'
        );

        const databaseName = `${this.props.tags?.environment}`;

        // configure RDS instance
        const rdsConfig: DatabaseInstanceProps = {
            engine: DatabaseInstanceEngine.postgres({version: PostgresEngineVersion.VER_12_3}),
            // optional, defaults to m5.large
            instanceType: InstanceType.of(InstanceClass.BURSTABLE2, InstanceSize.SMALL),
            instanceIdentifier: `${this.props.tags?.environment}-instance`,
            databaseName,
            vpc: vpc,
            vpcSubnets: {
                onePerAz: true,
                subnetType: SubnetType.PRIVATE
            },
            maxAllocatedStorage: 200,
            securityGroups: [databaseSecurityGroup],
            credentials: Credentials.fromSecret(this.databaseCredentialsSecret), // Get both username and password from existing secret
        }

        // create the instance
        const rdsInstance = new DatabaseInstance(this.scope, `${this.props.tags?.environment}Instance`, rdsConfig);
        // output the endpoint so we can connect!
        new CfnOutput(this.scope, 'RDS Endpoint', {value: rdsInstance.dbInstanceEndpointAddress});
        new CfnOutput(this.scope, 'Code Build Security Group Id', {value: codeBuildSecurityGroup.securityGroupId});
        const jdbcUrl = `jdbc:postgresql://${rdsInstance.dbInstanceEndpointAddress}:${rdsInstance.dbInstanceEndpointPort}/${databaseName}`;
        new CfnOutput(this.scope, 'JDBC URL', {value: jdbcUrl});

        return rdsInstance;
    }

}