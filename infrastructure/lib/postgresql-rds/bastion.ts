import {CdkEcsRdsStackProps} from "../cdk-ecs-rds-stack-props";
import * as cdk from "@aws-cdk/core";
import {Stack} from "@aws-cdk/core";
import {
    AmazonLinuxEdition,
    AmazonLinuxGeneration,
    AmazonLinuxStorage,
    AmazonLinuxVirt,
    Instance,
    InstanceType,
    IVpc,
    MachineImage,
    Peer,
    Port,
    SecurityGroup,
    Subnet,
    Vpc
} from "@aws-cdk/aws-ec2";

export class Bastion {
    constructor(private scope: Stack, private props: CdkEcsRdsStackProps) {
    }

    create(): SecurityGroup {
        // find vpc we want to put the RDS in
        const vpc = Vpc.fromLookup(this.scope, 'existingVpcBastion', {
            isDefault: false,
            tags: this.props.vpcTargetTag
        });

        // bastion security group
        const bastionSecurityGroup = new SecurityGroup(this.scope, `${this.props.tags?.environment}BastionSg`, {
            vpc,
            allowAllOutbound: true,
            description: `security group for ${this.props.tags?.environment} bastion host`,
            securityGroupName: `${this.props.tags?.environment}-bastion-sg`
        });
        const restrictedIp = this.props.restrictedIp ?? '0.0.0.0/0';
        bastionSecurityGroup.addIngressRule(Peer.ipv4(restrictedIp), Port.tcp(22));
        new BastionInstance(this.scope, this.props, bastionSecurityGroup, vpc).create();

        return bastionSecurityGroup;
    }
}

class BastionInstance {
    //should I look up the bastionSecurityGroup and vpc value based on name that would decouple from the bastion class?
    constructor(private scope: Stack,
                private props: CdkEcsRdsStackProps,
                private bastionSecurityGroup: SecurityGroup,
                private vpc: IVpc) {
    }

    create(): any {
        /**
         * create a bastion EC2 instance that has a public IP
         */
            // find existing public subnet
            // TODO: figure out why using fromSubnetId fails requiring availabilityZone on synth when set on the ec2 instance below.
        const subnetId = this.props.publicSubnet?.publicSubnetId + '';
        const existingPublicSubnet = Subnet.fromSubnetAttributes(this.scope, `${this.props.tags?.environment}BastionPublicSubnet`, {
            subnetId: subnetId,
            routeTableId: this.props.publicSubnet?.publicRouteTableId,
            availabilityZone: this.props.publicSubnet?.publicSubnetAz,
        });

        // Get AMI for bastion host
        // TODO: can this just be a constant?
        const amzLinux = MachineImage.latestAmazonLinux({
            generation: AmazonLinuxGeneration.AMAZON_LINUX_2,
            edition: AmazonLinuxEdition.STANDARD,
            virtualization: AmazonLinuxVirt.HVM,
            storage: AmazonLinuxStorage.GENERAL_PURPOSE
        });
        // Create instance type
        const t3Nano = new InstanceType('t3.nano');
        // Create Bastion instance
        const bastionHost = new Instance(this.scope, `${this.props.tags?.environment}BastionInstance`, {
            instanceType: t3Nano,
            vpc: this.vpc,
            machineImage: amzLinux,
            securityGroup: this.bastionSecurityGroup,
            keyName: this.props.bastionKeyName,
            // not specifying a public subnet could put it in a private one
            vpcSubnets: {
                subnets: [existingPublicSubnet]
            },
            instanceName: `${this.props.tags?.environment}-bastion`,
        });

        // output the bastion host endpoint so we can connect!
        new cdk.CfnOutput(this.scope, 'Bastion Host Endpoint', {value: bastionHost.instancePublicDnsName});
    }
}