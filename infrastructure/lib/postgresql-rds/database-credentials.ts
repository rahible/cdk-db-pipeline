import {CfnOutput, Stack} from "@aws-cdk/core";
import {CdkEcsRdsStackProps} from "../cdk-ecs-rds-stack-props";
import {Secret} from "@aws-cdk/aws-secretsmanager";
import {StringParameter} from "@aws-cdk/aws-ssm";

export class DatabaseCredentials {
    constructor(private scope: Stack, private props: CdkEcsRdsStackProps) {
    }

    create(): Secret {
        // Generate secrets to be used for password for the database
        const databaseCredentialsSecret = new Secret(this.scope, `${this.props.tags?.environment}DbCredentialsSecret`, {
            secretName: `${this.props.tags?.environment}-credentials`,
            generateSecretString: {
                secretStringTemplate: JSON.stringify({
                    username: 'postgres',
                }),
                excludePunctuation: true,
                includeSpace: false,
                generateStringKey: 'password'
            }
        });

        // lets output credentials location
        new CfnOutput(this.scope, 'Secret Name', {value: databaseCredentialsSecret.secretName});
        new CfnOutput(this.scope, 'Secret ARN', {value: databaseCredentialsSecret.secretArn});
        new CfnOutput(this.scope, 'Secret Full ARN', {value: databaseCredentialsSecret.secretFullArn || ''});

        // create a system parameter
        new StringParameter(this.scope, `${this.props.tags?.environment}CredentialsArn`, {
            parameterName: `${this.props.tags?.environment}-credentials-arn`,
            stringValue: databaseCredentialsSecret.secretArn
        });

        return databaseCredentialsSecret;
    }

}