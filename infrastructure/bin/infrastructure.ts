#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import {InfrastructureStack} from '../lib/infrastructure-stack';

const DEVELOPMENT = {
    env: {account: process.env.CDK_DEFAULT_ACCOUNT, region: process.env.CDK_DEFAULT_REGION},
    stackName: 'dev-db-rds',
    description: 'Development environment database stack.',
    bastionKeyName: 'CloudCmdKeyPair',
    publicSubnet: {
        publicSubnetId: 'subnet-0a5428b0215c25db5',
        publicRouteTableId: 'rtb-069a8baed8837e1f3',
        publicSubnetAz: 'us-east-1a'
    },
    vpcTargetTag: {
        'Name': 'aws-controltower-VPC'
    },
    tags: {
        environment: 'dev'
    }
};
const app = new cdk.App();
new InfrastructureStack(app, 'dev-InfrastructureStack', DEVELOPMENT);

const TEST = {
    env: {account: process.env.CDK_DEFAULT_ACCOUNT, region: process.env.CDK_DEFAULT_REGION},
    stackName: 'test-db-rds',
    description: 'Test environment database stack.',
    bastionKeyName: 'CloudCmdKeyPair',
    publicSubnet: {
        publicSubnetId: 'subnet-0a5428b0215c25db5',
        publicRouteTableId: 'rtb-069a8baed8837e1f3',
        publicSubnetAz: 'us-east-1a'
    },
    vpcTargetTag: {
        'Name': 'aws-controltower-VPC'
    },
    tags: {
        environment: 'test'
    }
};
new InfrastructureStack(app, 'test-InfrastructureStack', TEST);

const PRODUCTION = {
    env: {account: process.env.CDK_DEFAULT_ACCOUNT, region: process.env.CDK_DEFAULT_REGION},
    stackName: 'prod-db-rds',
    description: 'Production environment database stack.',
    bastionKeyName: 'CloudCmdKeyPair',
    publicSubnet: {
        publicSubnetId: 'subnet-0a5428b0215c25db5',
        publicRouteTableId: 'rtb-069a8baed8837e1f3',
        publicSubnetAz: 'us-east-1a'
    },
    vpcTargetTag: {
        'Name': 'aws-controltower-VPC'
    },
    tags: {
        environment: 'prod'
    }
};
new InfrastructureStack(app, 'prod-InfrastructureStack', PRODUCTION);
