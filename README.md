# Objective
Build out a AWS CDK pipeline to run db scripts using flyway on an existing RDS instance.

## Key Results
- When run, the pipeline will execute database scripts to build a schema, roles, users, tables and data.

## Intitiatives
- Use the blog on [building a continous delivery pipeline for database migrations](https://aws.amazon.com/blogs/database/building-a-cross-account-continuous-delivery-pipeline-for-database-migrations/) as an example.
- Create the RDS instance using cdk and secrets manager.
