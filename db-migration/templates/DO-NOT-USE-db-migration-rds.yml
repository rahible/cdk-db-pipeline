# Any code, applications, scripts, templates, proofs of concept, documentation
# and other items provided by AWS under this SOW are "AWS Content,"" as defined
# in the Agreement, and are provided for illustration purposes only. All such
# AWS Content is provided solely at the option of AWS, and is subject to the
# terms of the Addendum and the Agreement. Customer is solely responsible for
# using, deploying, testing, and supporting any code and applications provided
# by AWS under this SOW.

# (c) 2019 Amazon Web Services

Description: 'AWS CloudFormation Template AWS RDS Instance'
Parameters:
  TargetEnvironment:
    Type: String
    AllowedValues:
      - 'sandbox'
      - 'dev'
      - 'test'
      - 'staging'
      - 'prod'
    Description: Environment type
  ProjectName:
    Type: String
    Default: blogg
  DBInstanceClass:
    Default: db.t2.small
    Type: String
  StorageType:
    Default: gp2
    Type: String
  AllocatedStorage:
    Default: 20
    Type: Number
  RDSEngine:
    Default: postgres
    Type: String
  DBUser:
    Default: admin
    Type: String
  VpcId:
    Description: The ID of the VPC (e.g., vpc-0343606e).
    Type: AWS::EC2::VPC::Id
  PrivateSubnet1A:
    Description: The ID of the private subnet 1A in Availability Zone 1 (e.g., subnet-a0246dcd).
    Type: AWS::EC2::Subnet::Id
  PrivateSubnet1B:
    Description: The ID of the private subnet 1B in Availability Zone 2 (e.g., subnet-a0246dcd).
    Type: AWS::EC2::Subnet::Id
  MultiAZ:
    Default: "false"
    Description: Specify if Multi AZ (true or false)
    Type: String
  SshCidr:
    Default: "3.122.115.163/32"
    Description: CIDR for Bastion server used when connecting to database.
    Type: String
Resources:
  CodeBuildSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: "Security Group for CodeBuild Container,RDS security group must set to ingress source to CodeBuildSecurityGroup"
      VpcId: !Ref VpcId
      SecurityGroupEgress: #open for outbound traffic
        - IpProtocol: -1
          CidrIp: 0.0.0.0/0
      Tags:
        - Key: Name
          Value: !Join [ "-", [ !Ref 'ProjectName' , "flyway-codebuild-migration", !Ref 'AWS::Region' , !Ref 'TargetEnvironment','sg' ] ]

  RDSSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: RDS
      VpcId: !Ref VpcId
      SecurityGroupIngress:
        - Description: Bastion to RDS
          FromPort: 1433
          ToPort: 1433
          IpProtocol: tcp
          CidrIp: !Ref SshCidr
        - SourceSecurityGroupId: !Ref CodeBuildSecurityGroup
          IpProtocol: tcp
          FromPort: 1433
          ToPort: 1433
  RDSSubnetGroup:
    Type: "AWS::RDS::DBSubnetGroup"
    Properties:
      DBSubnetGroupDescription: "Default DB Subnet group for RDS created from cloudformation"
      SubnetIds:
        - !Ref PrivateSubnet1A
        - !Ref PrivateSubnet1B
  RDSInstance:
    Type: "AWS::RDS::DBInstance"
    Properties:
      DBInstanceIdentifier: !Join [ "-", [ !Ref 'ProjectName' , "rds-instance", !Ref 'RDSEngine', !Ref 'AWS::Region' , !Ref 'TargetEnvironment' ] ]
      AllocatedStorage: !Ref AllocatedStorage
      StorageType: !Ref StorageType
      DBInstanceClass: !Ref DBInstanceClass
      Engine: !Ref RDSEngine
      MasterUsername: !Join [ '', [ '{{resolve:secretsmanager:', !Ref RDSInstanceSecret, ':SecretString:username}}' ] ]
      MasterUserPassword: !Join [ '', [ '{{resolve:secretsmanager:', !Ref RDSInstanceSecret, ':SecretString:password}}' ] ]
      VPCSecurityGroups:
        - !GetAtt RDSSecurityGroup.GroupId
      DBSubnetGroupName: !Ref RDSSubnetGroup
      PubliclyAccessible: false
      MultiAZ: !Ref MultiAZ
    DeletionPolicy: "Snapshot"
  RDSInstanceSecret:
    Type: AWS::SecretsManager::Secret
    Properties:
      Description: 'RDS instance secret'
      Name: "rdsdb"
      GenerateSecretString:
        SecretStringTemplate: !Join [ '',[ '{"username": "', !Ref DBUser, '"}' ] ]
        GenerateStringKey: 'password'
        PasswordLength: 16
        ExcludePunctuation: "true"
  SecretRDSInstanceAttachment:
    Type: AWS::SecretsManager::SecretTargetAttachment
    Properties:
      SecretId: !Ref RDSInstanceSecret
      TargetId: !Ref RDSInstance
      TargetType: AWS::RDS::DBInstance
Outputs:
  CodeBuildSecurityGroupId:
    Description: SecurityGroup for codebuild container
    Value: !GetAtt CodeBuildSecurityGroup.GroupId
  DatabaseEndpoint:
    Description: JDBC database endpoint.
    Value:
      Fn::Join:
        - ""
        - - "jdbc:sqlserver://"
          - !GetAtt [ RDSInstance, Endpoint.Address ]
          - ":1433;databaseName=tempdb"    